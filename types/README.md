# Types of Microformats

This provides a collection of types that can be used to handle in-memory representation of 
Microformats as well as simplifying the representation of them in other formats (like JSON).
