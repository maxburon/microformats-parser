use super::*;
use crate::parse::{
    element::test::{from_html_str, grab_element_from_document},
    ElementRef, MatchedElements,
};
use microformats_types::KnownClass;
use tracing_test::traced_test;
use tracing_unwrap::OptionExt;

#[traced_test]
#[test]
fn plain() -> Result<(), crate::parse::Error> {
    let plain_text_dom = from_html_str(
        r#"<div class='h-entry'><a href='/foo' class='p-author h-card'>a <span class='p-tag'>place</span></a></div>"#,
    );
    let plain_text_elem = grab_element_from_document(&plain_text_dom, "a").unwrap_or_log();

    let plain_text_parser = PropertyParser::new(
        Arc::new(ElementRef {
            index: 0,
            node: Node {
                elem: plain_text_elem,
            },
        }),
        &"http://example.com".parse()?,
    );

    let plain_text_history = MatchedElements::for_document(&plain_text_dom)?;

    similar_asserts::assert_serde_eq!(
        parsed: plain_text_parser.expand(&plain_text_history)?,
        expected: Properties::from_iter( vec![ (
            "author".to_string(),
            vec![PropertyValue::Item(Item {
                r#type: vec![KnownClass::Card.into()],
                properties: Properties::from_iter(vec![
                    (
                        "tag".to_string(),
                        vec![PropertyValue::Plain("place".to_string())]
                    ),
                    (
                        "url".to_string(),
                        vec![PropertyValue::Url("http://example.com/foo".parse()?)]
                    )
                ]),
                value: Some(ValueKind::Plain("a place".to_string())),
                ..Default::default()
            })]
        ) ]),
        "expanded a direct plain property"
    );

    Ok(())
}

#[traced_test]
#[test]
fn linked() -> Result<(), crate::parse::Error> {
    let linked_dom = from_html_str(
        r#"<div class='h-entry'><a href='/foo' class='u-author h-card'>a <span class='p-name'>place</span></a></div>"#,
    );
    let linked_elem = grab_element_from_document(&linked_dom, "a").unwrap_or_log();

    let linked_parser = PropertyParser::new(
        Arc::new(ElementRef {
            index: 0,
            node: Node { elem: linked_elem },
        }),
        &"http://example.com".parse()?,
    );

    let linked_history = MatchedElements::for_document(&linked_dom)?;

    similar_asserts::assert_serde_eq!(
        parsed: linked_parser.expand(&linked_history)?,
        expected: Properties::from_iter( vec![ (
            "author".to_string(),
            vec![PropertyValue::Item(Item {
                r#type: vec![KnownClass::Card.into()],
                properties: Properties::from_iter(vec![
                    (
                        "name".to_string(),
                        vec![PropertyValue::Plain("place".to_string())]
                    ),
                    (
                        "url".to_string(),
                        vec![PropertyValue::Url("http://example.com/foo".parse()?)]
                    )
                ]),
                value: Some(ValueKind::Url("http://example.com/foo".parse()?)),
                ..Default::default()
            })]
        ) ]),
        "expanded a direct linked property"
    );
    Ok(())
}
