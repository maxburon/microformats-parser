#![cfg(test)]

use crate::parse::Parser;
use microformats_types::Document;
mod suite;

#[yare::parameterized(
    sample = { r#"
<!DOCTYPE html>
<html>
    <head>
        <base href="https://example.com" />
    </head>
    <body>
        <main class="h-feed" id="feed">
            A pretty simple feed.
            <img src="/photo.jpg" alt="the photo" />
            <div class="h-entry" id="entry1">
                An initial entry.
            </div>
            <div class="h-entry" id="entry2">
                <span class="p-name">A title.</span>
                <a href="/jump-uid" class="u-uid"></a>
                <a href="/jump-url" class="u-url"></a>
            </div>
            <div class="h-entry" id="entry3">
                <span class="p-name">Another title.</span>
            </div>
            <a href="/"></a>
        </main>
        <aside id="selfReview" class="h-review"></aside>
    </body>
</html>
        "#, 
        serde_json::from_value(serde_json::json!({
            "items": [
                {
                    "id": "feed",
                    "type": ["h-feed"],
                    "properties": {},
                    "children": [
                        {
                            "id": "entry1",
                            "type": ["h-entry"],
                            "properties": {
                                "name": ["An initial entry."],
                            },
                        },
                        {
                            "id": "entry2",
                            "type": ["h-entry"],
                            "properties": {
                                "name": ["A title."],
                                "uid": ["https://example.com/jump-uid"],
                                "url": ["https://example.com/jump-url"],
                            },
                        },
                        {
                            "id": "entry3",
                            "type": ["h-entry"],
                            "properties": {
                                "name": ["Another title."]
                            },
                        }
                    ]
                },
                {
                    "type": ["h-review"],
                    "id": "selfReview",
                    "properties": {},
                }
            ],
            "rels": {},
            "rel-urls": {}
        })).unwrap()
    }
)]
fn parse_into_document(html: &str, document: Document) -> Result<(), crate::Error> {
    let parser = Parser::from_html(html.to_string())?;

    let result = parser.into_document(Some("http://example.com".parse()?));

    similar_asserts::assert_serde_eq!(
        actual: result.map_err(|e| e.to_string()),
        expected: Ok(document),
        "forms expected document"
    );

    Ok(())
}
